package com.globallogic.webinar.wishlist.integration;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WishListTestRestAssured {

    @LocalServerPort
    private int port;

    @Test
    void getWishes() {
        given()
                .port(port)
                .when()
                .get("/")
                .then()
                .statusCode(200)
                .body("", hasSize(0));
    }

    @Test
    void lifecycleTest() {
        given()
                .body("Playstation 5")
                .port(port)
                .when()
                .post("/")
                .then()
                .statusCode(200);

        given()
                .port(port)
                .when()
                .get("/")
                .then()
                .statusCode(200)
                .body("", hasSize(1))
                .body("", Matchers.contains("Playstation 5"));

        given()
                .body("Playstation 5")
                .port(port)
                .when()
                .delete("/")
                .then()
                .statusCode(200);

        given()
                .port(port)
                .when()
                .get("/")
                .then()
                .statusCode(200)
                .body("", hasSize(0));
    }
}
