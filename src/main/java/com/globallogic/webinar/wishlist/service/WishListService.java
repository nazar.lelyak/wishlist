package com.globallogic.webinar.wishlist.service;

import com.globallogic.webinar.wishlist.entity.WishEntity;
import com.globallogic.webinar.wishlist.repository.WishListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class WishListService {

    private final WishListRepository repository;

    public void addWish(String wish) {
        WishEntity wishEntity = new WishEntity();
        wishEntity.setWish(wish);
        repository.save(wishEntity);
    }

    public Set<String> getWishes() {
        return StreamSupport.stream(repository.findAll().spliterator(), false)
                .map(WishEntity::getWish)
                .collect(Collectors.toSet());
    }

    @Transactional
    public void deleteWish(String wish) {
        repository.deleteByWish(wish);
    }
}
