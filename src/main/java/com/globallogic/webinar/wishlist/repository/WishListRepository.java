package com.globallogic.webinar.wishlist.repository;

import com.globallogic.webinar.wishlist.entity.WishEntity;
import org.springframework.data.repository.CrudRepository;

public interface WishListRepository extends CrudRepository<WishEntity, Integer> {
    void deleteByWish(String wish);
}

