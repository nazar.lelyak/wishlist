package com.globallogic.webinar.wishlist.controller;

import com.globallogic.webinar.wishlist.service.WishListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequiredArgsConstructor
public class WishListController {

    private final WishListService wishListService;

    @PostMapping
    public void addWish(@RequestBody String wish) {
        wishListService.addWish(wish);
    }

    @GetMapping
    public Set<String> getWishes() {
        return wishListService.getWishes();
    }

    @DeleteMapping
    public void removeWish(@RequestBody String wish) {
        wishListService.deleteWish(wish);
    }
}
